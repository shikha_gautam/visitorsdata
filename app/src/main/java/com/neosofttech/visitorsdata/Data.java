
package com.neosofttech.visitorsdata;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("card_id")
    @Expose
    private String cardId;
    @SerializedName("card_name")
    @Expose
    private String cardName;

    /**
     *
     * @return
     * The cardId
     */
    public String getCardId() {
        return cardId;
    }

    /**
     *
     * @param cardId
     * The card_id
     */
    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    /**
     *
     * @return
     * The cardName
     */
    public String getCardName() {
        return cardName;
    }

    /**
     *
     * @param cardName
     * The card_name
     */
    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

}