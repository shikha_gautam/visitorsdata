package com.neosofttech.visitorsdata;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.neosofttech.visitorsdata.databaseOp.DatabaseHandler;
import com.neosofttech.visitorsdata.databaseOp.DatabaseManager;
import com.neosofttech.visitorsdata.databaseOp.DatabaseUtils;

import java.io.File;

/**
 * Created by webwerks on 18/9/15.
 */
public class VisitorUpdateActivity extends Activity implements View.OnClickListener {

    int entryId;

    private static final  int FROM_CAMERA_1 = 12,
            FROM_CAMERA_2 = 13,
            FROM_GALLERY_1 = 14 ,
            FROM_GALLERY_2 = 15,
            SCAN_COMPLETE_1 = 16,
            SCAN_COMPLETE_2 = 17  ;

    TextView textView,textView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.visitor_detail);


        findViewById(R.id.imgBack).setOnClickListener(new
                                                              View.OnClickListener() {
                                                                  @Override
                                                                  public void onClick(View v) {
                                                                      onBackPressed();
                                                                  }
                                                              });

        entryId = Integer.parseInt(getIntent().getStringExtra("EntryId"));

        (textView = (TextView)findViewById(R.id.deleteAll)).setText("Save");
        (textView1 = (TextView)findViewById(R.id.txtUserName)).setText("Back");

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        textView.setOnClickListener(this);
        //textView.setVisibility(View.VISIBLE);

        init();

        pickPic1.setOnClickListener(this);
        pickPic2.setOnClickListener(this);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                textView.setVisibility(View.VISIBLE);
            }
        });

    }


    Uri galleryURi1,galleryUri2;
    ImageView pickPic1, pickPic2;
    EditText editText;

    private void init(){

        pickPic1 = (ImageView)findViewById(R.id.imageView1);
        pickPic2 = (ImageView)findViewById(R.id.imageView2);

        String query = " select * from " + DatabaseUtils.VISITORS_INFO_TABLE + " where " + DatabaseUtils.VISTORS_ENTRY_ID + " = " + entryId;

        SQLiteDatabase sqLiteDatabase = new DatabaseHandler(this).getReadableDatabase();

        Cursor cursor
                 = sqLiteDatabase.rawQuery(query,null);

        if (cursor.moveToFirst()){

            String imageUrl = cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_EVENT_IMAGE_URL1));
            String imageUrl2 = cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_EVENT_IMAGE_URL2));


            Log.e("image url " , " image url " + imageUrl + "   " + imageUrl2);
            ((TextView)findViewById(R.id.txtTitle)).setText(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_NAME)));
            if (imageUrl.length()>1){


                Log.e("query", " image view 1 " + imageUrl);
                galleryURi1 = Uri.parse(imageUrl);
                //setImageUsingUri(galleryURi1,pickPic1);

                 getImageUrl(imageUrl, pickPic1);
            }

            if (imageUrl2.length()>1){

                Log.e("query " , " image view 2 " + imageUrl2);
                galleryUri2 = Uri.parse(imageUrl2);
                getImageUrl(imageUrl2, pickPic2);

            }
        }

        (editText = (EditText)findViewById(R.id.commentText)).setText(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_COMENTS_TEXT)) + "");

    }


    @Override
    public void onClick(View v) {


        switch (v.getId()){
            case R.id.imageView1 :
                textView.setVisibility(View.VISIBLE);
                showImagePicDialog(FROM_GALLERY_1,FROM_CAMERA_1);
                break;
            case R.id.imageView2 :
                textView.setVisibility(View.VISIBLE);
                showImagePicDialog(FROM_GALLERY_2, FROM_CAMERA_2);
                break;

            case R.id.deleteAll :

                if (textView.getVisibility() == View.VISIBLE)
                    updateDB();
                break;

            case R.id.txtUserName :
                onBackPressed();
                break;



        }
    }

    private void updateDB() {

        String url1, url2 ;
        if (galleryURi1!=null){
            url1 = galleryURi1.toString();
        }else{
            url1 = "";
        }

        if (galleryUri2 != null){
            url2 = galleryUri2.toString();
        }else{
            url2 = "";
        }

        Log.e(" url " , " url address " + url1 + "   " + url2  ) ;

        String query = " update " + DatabaseUtils.VISITORS_INFO_TABLE + " set " +
                                DatabaseUtils.VISITORS_EVENT_IMAGE_URL1 + " = '" + url1 + "' , " +
                                DatabaseUtils.VISITORS_EVENT_IMAGE_URL2+ " = '"+ url2 +"' , "+
                                DatabaseUtils.VISITORS_COMENTS_TEXT + " = '" + editText.getText().toString()+"', " +
                                DatabaseUtils.VISITORS_IS_SYNCED + " = 0 "+
                                " where " + DatabaseUtils.VISTORS_ENTRY_ID + " = " +entryId;

        Log.e(" update query ", query);

        DatabaseHandler databaseHandler = new DatabaseHandler(this);
        DatabaseManager.initializeInstance(databaseHandler);

        SQLiteDatabase sqLiteDatabase = DatabaseManager.getInstance().openDatabase();

        sqLiteDatabase.execSQL(query);

        DatabaseManager.getInstance().closeDatabase();

        Toast.makeText(this," Saved data ", Toast.LENGTH_LONG).show();
    }


    private String getImageUrl(String imageUrl,ImageView imageView){
        if(imageUrl.contains("file://")){
            imageUrl = imageUrl.replace("file://","");
            BitmapUtils.setImage(imageView,imageUrl);

        }else if (imageUrl.contains("content:/")){
            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(Uri.parse(imageUrl), filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            cursor.close();
            //mFeedbackBitmap = BitmapFactory.decodeFile(imagePath);
            Bitmap bitmap = BitmapUtils.setImage(imageView, imagePath);
        }
        return imageUrl;
    }


    private void showImagePicDialog(final  int requestCodeForGallery, final int requestCodeForCamera) {
        final Dialog dialog = new Dialog(this, R.style.DialogSlideAnim);
        dialog.setContentView(R.layout.dialog_image_picker);
        dialog.setCanceledOnTouchOutside(false);

        final TextView showGallery, showImage;
        final ImageView cancel;

        showGallery = (TextView)dialog.findViewById(R.id.txtOpenGallery);
        showImage = (TextView)dialog.findViewById(R.id.txtTakePhoto);

        cancel = (ImageView)dialog.findViewById(R.id.imgCancel);

        showGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGallery(requestCodeForGallery);
                dialog.dismiss();
            }
        });

        showImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                captureImage(requestCodeForCamera);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    private void captureImage(int requestCode) {
        Intent intent      = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile   = FileUtils.getImageFile();


        Uri uri   = Uri.fromFile(photoFile);
        if (requestCode == FROM_CAMERA_1 ){
            galleryURi1 = uri;
        }else if (requestCode == FROM_CAMERA_2 ){
            galleryUri2 = uri;
        }


        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, requestCode);
    }

    private void pickImageFromGallery(int requestCode) {
        startActivityForResult(new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI),
                requestCode);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("requestCode:", requestCode + ",resultCode:" + resultCode);




        if(requestCode == FROM_CAMERA_1 && resultCode == RESULT_OK) {
            //mFeedbackBitmap = BitmapFactory.decodeFile(mUriImage.getPath());
            Bitmap mFeedbackBitmap = BitmapUtils.setImage(pickPic1, galleryURi1.getPath());
            if(mFeedbackBitmap == null) {
                // showToast("Image not found.");
                return;
            }
            Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScannerIntent.setData(galleryURi1);
            sendBroadcast(mediaScannerIntent);


        } else if(requestCode == FROM_GALLERY_1 && resultCode == FragmentActivity.RESULT_OK) {

            galleryURi1 = data.getData();


            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(galleryURi1, filePath, null, null, null);
            cursor.moveToFirst();

            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            cursor.close();
            //mFeedbackBitmap = BitmapFactory.decodeFile(imagePath);
            Bitmap bitmap = BitmapUtils.setImage(pickPic1, imagePath);
            if(bitmap == null) {

                return;
            }

        }

        if(requestCode == FROM_CAMERA_2 && resultCode == RESULT_OK) {

            Bitmap mFeedbackBitmap = BitmapUtils.setImage(pickPic2, galleryUri2.getPath());
            if(mFeedbackBitmap == null) {

                return;
            }
            Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScannerIntent.setData(galleryUri2);
            sendBroadcast(mediaScannerIntent);


        } else if(requestCode == FROM_GALLERY_2 && resultCode == FragmentActivity.RESULT_OK) {
            galleryUri2 = data.getData();


            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(galleryUri2, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            cursor.close();
            //mFeedbackBitmap = BitmapFactory.decodeFile(imagePath);
            Bitmap bitmap = BitmapUtils.setImage(pickPic2, imagePath);
            if(bitmap == null) {
                //showToast("Image not found.");
                return;
            }
            //mImgFeedback.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        }

        else if(resultCode == FragmentActivity.RESULT_OK && requestCode == SCAN_COMPLETE_2 && data != null) {
            if(data.getStringExtra("SCAN_RESULT") != null) {
                Log.d("SCAN", data.getStringExtra("SCAN_RESULT").toString());
                // mImgBarcodeCaptured.setVisibility(View.VISIBLE);
                // mBtnBarcode.setTag(data.getStringExtra("SCAN_RESULT").toString());
            }
        }
        else if(resultCode == FragmentActivity.RESULT_OK && requestCode == SCAN_COMPLETE_1 && data != null) {
            if(data.getStringExtra("SCAN_RESULT") != null) {
                Log.d("SCAN", data.getStringExtra("SCAN_RESULT").toString());
                // mImgBarcodeCaptured.setVisibility(View.VISIBLE);
                // mBtnBarcode.setTag(data.getStringExtra("SCAN_RESULT").toString());
            }
        }
    }


}
