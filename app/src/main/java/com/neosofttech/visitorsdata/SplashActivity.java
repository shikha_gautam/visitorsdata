package com.neosofttech.visitorsdata;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.GsonBuilder;
import com.neosofttech.visitorsdata.databaseOp.DatabaseHandler;
import com.neosofttech.visitorsdata.databaseOp.DatabaseManager;
import com.neosofttech.visitorsdata.databaseOp.DatabaseUtils;

import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by webwerks on 29/9/15.
 */
public class SplashActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.splash_actiivity);

        if (NetworkUtil.isNetworkAvailable(this)){
            try{
                new Loaduser1().execute();
            }catch (Exception e){
                e.printStackTrace();
                startLoginScreen();
            }
        }else{
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startLoginScreen();
                }
            },1500);
        }

    }


    private class Loaduser1 extends AsyncTask<Void,Void,Void>{

        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(SplashActivity.this);
            progressDialog.setMessage("Loading..");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            startLoginScreen();
        }

        @Override
        protected Void doInBackground(Void... params) {

            LoadUser loadUser = new LoadUser(SplashActivity.this);
            return null;
        }
    }

    private void startLoginScreen(){
        startActivity(new Intent(this, LoginScreen.class));
        finish();
    }
}
