package com.neosofttech.visitorsdata;

/**
 * Created by webwerks on 18/9/15.
 */
public class EventPojo {

    private int id;
    private String name;

    public EventPojo(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
