package com.neosofttech.visitorsdata;

import android.util.Log;

import com.google.gson.GsonBuilder;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 6/10/15.
 */
public class ResetPasswordApi {

    public ResetPasswordApi(){


    }

    public String resetPassword(int userId, String oldPassword, String newPassword){

        String strUrl = "http://neo.showcase-url.com/index.php/sync/reset_password";

        try {
            URL url = new URL(strUrl);

            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setConnectTimeout(2000);

            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            httpURLConnection.setRequestProperty("charset", "utf-8");

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("user_id",""+ userId));
            params.add(new BasicNameValuePair("old_password",oldPassword));
            params.add(new BasicNameValuePair("new_password",newPassword));

            OutputStream os = httpURLConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(params));

            writer.flush();
            writer.close();
            os.close();

            httpURLConnection.connect();

            InputStream inputStream = httpURLConnection.getInputStream();

            return  feedToChange(inputStream);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {

        StringBuilder result = new StringBuilder();

        boolean first = true;

        for (NameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");

            result.append(URLEncoder.encode(pair.getValue() == null ? "" : pair.getValue(), "UTF-8"));
        }

        Log.e(" param value ", result.toString());
        return result.toString();
    }


    private String feedToChange(InputStream inputStream){

        ResetModel resetModel = new GsonBuilder().create().fromJson(new InputStreamReader(inputStream),ResetModel.class);

        return resetModel.getMessage();
    }

}
