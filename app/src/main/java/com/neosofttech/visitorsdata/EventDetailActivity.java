package com.neosofttech.visitorsdata;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.neosofttech.visitorsdata.databaseOp.DatabaseHandler;
import com.neosofttech.visitorsdata.databaseOp.DatabaseManager;
import com.neosofttech.visitorsdata.databaseOp.DatabaseUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by webwerks on 18/9/15.
 */


public class EventDetailActivity extends Activity {

    ListView listView;
    String eventName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_activity_layout);

        (findViewById(R.id.imgBack)).setOnClickListener(new
                                                                View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        onBackPressed();
                                                                    }
                                                                });

        (findViewById(R.id.deleteAll)).setVisibility(View.VISIBLE);

        ((TextView)findViewById(R.id.txtUserName)).setText("Events");

        findViewById(R.id.txtUserName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        eventName = getIntent().getStringExtra("EventName");
        String eventName1 = eventName;
        if (eventName.length()>10){
            eventName1 = eventName.substring(0,8)+"...";
        }

        ((TextView)findViewById(R.id.txtTitle)).setText(eventName1);
        listView = (ListView) findViewById(R.id.list);

       // init();

        (findViewById(R.id.deleteAll)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAll();
            }


        });


    }

    private void deleteAll(){

        new AlertDialog.Builder(EventDetailActivity.this)
                .setTitle("Data Inserted")
                .setMessage("You data is saved in database")
                .setPositiveButton("Delete All", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String query = " delete  from " + DatabaseUtils.VISITORS_INFO_TABLE + " where " + DatabaseUtils.VISITORS_EVENT_NAME + " = '" + eventName+ "' and " + DatabaseUtils.VISITOR_USER_ID_FK + " = " + LoginScreen.UserId ;

                        DatabaseHandler databaseHandler = new DatabaseHandler(EventDetailActivity.this);

                        DatabaseManager.initializeInstance(databaseHandler);

                        SQLiteDatabase sqLiteDatabase = DatabaseManager.getInstance().openDatabase();

                        sqLiteDatabase.execSQL(query);

                        DatabaseManager.getInstance().closeDatabase();

                        init();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();




    }

    List<String> visitorName;

    HashMap<Integer, String> visitorDetail;

    int eventId;


    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    public void init(){


        visitorDetail = new HashMap<>();
        visitorName = new ArrayList<>();

        String query = " select * from " + DatabaseUtils.EVENTS_TABLE + " where " + DatabaseUtils.EVENT_NAME+  " = '" + eventName + "' ";

        SQLiteDatabase sqLiteDatabase = new DatabaseHandler(this).getReadableDatabase();

        Cursor cursor = sqLiteDatabase.rawQuery(query,null);

        if (cursor.moveToFirst()){

            eventId = cursor.getInt(cursor.getColumnIndex(DatabaseUtils.EVENT_ID));

            query = " select * from "+ DatabaseUtils.VISITORS_INFO_TABLE + " where " + DatabaseUtils.VISITORS_EVENT_ID + " = " +eventId  ;

            cursor = sqLiteDatabase.rawQuery(query,null);

            if(cursor.moveToFirst()){

                for (int pos = 0 ; pos< cursor.getCount(); pos++) {

                    Log.e("cursor ", " cursor " + cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_NAME)) + "  " + cursor.getInt(cursor.getColumnIndex(DatabaseUtils.VISITORS_IS_SYNCED)) + " &&&& " + cursor.getInt(cursor.getColumnIndex(DatabaseUtils.VISTORS_ENTRY_ID)));

                    cursor.moveToPosition(pos);

                    visitorName.add(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_NAME)));
                    visitorDetail.put(pos, cursor.getInt(cursor.getColumnIndex(DatabaseUtils.VISITORS_IS_SYNCED))+ " &&&& " + cursor.getInt(cursor.getColumnIndex(DatabaseUtils.VISTORS_ENTRY_ID)));



                }
            }


        }

        if (cursor.getCount() == 0 ){
            (findViewById(R.id.deleteAll)).setVisibility(View.GONE);
        }

        ListItemAdapter listItemAdapter = new ListItemAdapter(this,visitorName);

        listView.setAdapter(listItemAdapter);


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


                return true;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            String entryId = visitorDetail.get(position).split(" &&&& ")[1];

                Intent intent
                         = new Intent(EventDetailActivity.this,VisitorUpdateActivity.class);

                intent.putExtra("EntryId", entryId);
                startActivity(intent);


            }
        });
    }





    private class DeleteData extends AsyncTask<Void,Void,Void>{

        ProgressDialog progressDialog;

        int position ;
        public DeleteData(int pos){
            this.position = pos;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(EventDetailActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage("deleting data");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String query = " delete from " + DatabaseUtils.VISITORS_INFO_TABLE + " where " + DatabaseUtils.VISTORS_ENTRY_ID + " = " +visitorDetail.get(position).split(" &&&& ")[1];


            DatabaseHandler databaseHandler = new DatabaseHandler(EventDetailActivity.this);
            DatabaseManager.initializeInstance(databaseHandler);

            SQLiteDatabase sqLiteDatabase = DatabaseManager.getInstance().openDatabase();

            sqLiteDatabase.execSQL(query);

            DatabaseManager.getInstance().closeDatabase();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing() ){
                progressDialog.dismiss();
            }


            init();
        }
    }

    private class StartSync extends AsyncTask<Void,Void,Void> {


        String value ;
        public StartSync(String value){
            this.value = value;
        }
        ProgressDialog progressDialog;

        String message;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(EventDetailActivity.this);

            progressDialog.setCancelable(false);
            progressDialog.setMessage(" syncing data");
            progressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... params) {

            SQLiteDatabase sqLiteDatabase = new DatabaseHandler(EventDetailActivity.this).getReadableDatabase();

            String query = " select * from " + DatabaseUtils.VISITORS_INFO_TABLE + " where "
                    + DatabaseUtils.VISITORS_IS_SYNCED + " = 0 " + " and " + DatabaseUtils.VISTORS_ENTRY_ID + " = " + value;

            Cursor cursor = sqLiteDatabase.rawQuery(query,null);

            if (cursor != null && cursor.moveToFirst()){

                for (int i =0 ; i <cursor.getCount() ; i++){

                    VisitorData visitorData = new VisitorData();
                    visitorData.setVisitorname(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_NAME)));
                    visitorData.setComentText(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_COMENTS_TEXT)));
                    visitorData.setImageUrl1(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_EVENT_IMAGE_URL1)));
                    visitorData.setImageUrl2(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_EVENT_IMAGE_URL2)));
                    visitorData.setEventId(cursor.getInt(cursor.getColumnIndex(DatabaseUtils.VISITORS_EVENT_ID)));
                    visitorData.setEntryId(cursor.getInt(cursor.getColumnIndex(DatabaseUtils.VISTORS_ENTRY_ID)));
                    SyncData syncData = new SyncData(visitorData, EventDetailActivity.this);

                    message = syncData.startSync();
                }

            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (progressDialog.isShowing()){
                progressDialog.dismiss();
            }

            new AlertDialog.Builder(EventDetailActivity.this).setMessage(message).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();

            init();
        }

    }


    public class ListItemAdapter extends BaseAdapter {

        private Context mContext;
        private List<String> list;
        private LayoutInflater mInflater;

        public ListItemAdapter(Context mContext, List<String> dataList) {
            this.mContext = mContext;
            this.list = dataList;
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.layout_list_item1, null);
            }

            TextView txtUserName = (TextView) convertView.findViewById(R.id.txtUserName);
            TextView txtSyncState = (TextView) convertView.findViewById(R.id.txtSyncState);
            TextView txtCount = (TextView) convertView.findViewById(R.id.txtCount);

            txtUserName.setText(visitorName.get(position));

            txtSyncState.setText(visitorDetail.get(position).split(" &&&& ")[0].contains("0") ? "Not synced":" Synced" );
            txtCount.setText(visitorDetail.get(position).split(" &&&& ")[1]);
            (convertView.findViewById(R.id.sync)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("selected ", " selected position " + position);
                    new StartSync(visitorDetail.get(position).split(" &&&& ")[1]).execute();

                }
            });

            convertView.findViewById(R.id.delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    new DeleteData(position).execute();
                }
            });
            return convertView;
        }


    }
}