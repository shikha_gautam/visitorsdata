package com.neosofttech.visitorsdata;

/**
 * Created by webwerks on 18/9/15.
 */
public class VisitorData {

    int visitorId,eventId,entryId, isSync, visitorRating;
    String visitorname, eventName, imageUrl1, imageUrl2, date,comentText;

    public VisitorData(){

    }


    public int getVisitorRating() {
        return visitorRating;
    }

    public void setVisitorRating(int visitorRating) {
        this.visitorRating = visitorRating;
    }

    public int getEntryId() {
        return entryId;
    }

    public int getEventId() {
        return eventId;
    }

    public int getIsSync() {
        return isSync;
    }

    public int getVisitorId() {
        return visitorId;
    }

    public String getComentText() {
        return comentText;
    }

    public String getDate() {
        return date;
    }

    public String getEventName() {
        return eventName;
    }

    public String getImageUrl1() {
        return imageUrl1;
    }

    public String getImageUrl2() {
        return imageUrl2;
    }

    public String getVisitorname() {
        return visitorname;
    }


    public void setComentText(String comentText) {
        this.comentText = comentText;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setImageUrl1(String imageUrl1) {
        this.imageUrl1 = imageUrl1;
    }

    public void setImageUrl2(String imageUrl2) {
        this.imageUrl2 = imageUrl2;
    }

    public void setIsSync(int isSync) {
        this.isSync = isSync;
    }

    public void setVisitorId(int visitorId) {
        this.visitorId = visitorId;
    }

    public void setVisitorname(String visitorname) {
        this.visitorname = visitorname;
    }




}
