package com.neosofttech.visitorsdata;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.GsonBuilder;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.neosofttech.visitorsdata.databaseOp.DatabaseHandler;
import com.neosofttech.visitorsdata.databaseOp.DatabaseManager;
import com.neosofttech.visitorsdata.databaseOp.DatabaseUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by webwerks on 18/9/15.
 */
public class EventActivity extends Activity implements View.OnClickListener {


    private Button submitButton;
    private ImageView pickPic1, pickPic2;
    private EditText eventName,commentText;
    private TextView syncText, cancelButton;


    private static final  int FROM_CAMERA_1 = 12,
            FROM_CAMERA_2 = 13,
            FROM_GALLERY_1 = 14 ,
            FROM_GALLERY_2 = 15,
            SCAN_COMPLETE_1 = 16,
            SCAN_COMPLETE_2 = 17  ;

    private Uri galleryURi1, galleryUri2;
    private Spinner eventSpinner;
    private RatingBar eventRatingBar;
    private View parentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_event);

        parentView = findViewById(R.id.fullScreen);


        SharedPreferences sharedPreferences = getSharedPreferences(Constant.SHARED_PREFERENCE_CONST, Context.MODE_PRIVATE);
        String userEmail = sharedPreferences.getString("userEmail","");
        if (userEmail.contains("@"))
            ((TextView)findViewById(R.id.userName)).setText(userEmail.split("@")[0]);

        submitButton = (Button)findViewById(R.id.saveButton);
        cancelButton = (TextView)findViewById(R.id.cancel);


        syncText = (TextView)findViewById(R.id.sync);

        syncText.setOnClickListener(this);
        pickPic1 = (ImageView)findViewById(R.id.findImageScreen1);
        pickPic2 = (ImageView)findViewById(R.id.findImageScreen2);

        eventName = (EditText)findViewById(R.id.eventName);
        commentText = (EditText)findViewById(R.id.addComment);

        pickPic1.setOnClickListener(this);
        pickPic2.setOnClickListener(this);

        submitButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);



        (findViewById(R.id.list)).setOnClickListener(this);


        eventSpinner = (Spinner)findViewById(R.id.eventSpinner);

        eventRatingBar = (RatingBar)findViewById(R.id.ratingBar);
        eventRatingBar.setOnClickListener(this);

        // init();

        new GetEvent().execute();



    }





    ArrayList<EventPojo> eventPojos = new ArrayList<>();
    private void init(){

        String query = " select * from " +DatabaseUtils.EVENTS_TABLE ;



        Cursor cursor
                = new DatabaseHandler(this).getReadableDatabase().rawQuery(query,null);

        if (cursor != null && cursor.moveToFirst()){

            for (int i = 0 ; i < cursor.getCount() ;i++){

                cursor.moveToPosition(i);

                EventPojo eventPojo = new EventPojo();
                eventPojo.setName(cursor.getString(cursor.getColumnIndex(DatabaseUtils.EVENT_NAME)));
                eventPojo.setId(cursor.getInt(cursor.getColumnIndex(DatabaseUtils.EVENT_ID)));

                eventPojos.add(eventPojo);



            }
        }

        ArrayList<String> eventData = new ArrayList<>();
        for (int i = 0 ; i <eventPojos.size() ;i++){
            eventData.add(eventPojos.get(i).getName());
        }


        ArrayAdapter<String> eventPojoArrayAdapter = new ArrayAdapter<String>(EventActivity.this,android.R.layout.simple_spinner_item,eventData);

        eventPojoArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        eventSpinner.setAdapter(eventPojoArrayAdapter);
    }


    private void captureImage(int requestCode) {
        Intent intent      = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile   = FileUtils.getImageFile();


        Uri uri   = Uri.fromFile(photoFile);
        if (requestCode == FROM_CAMERA_1 ){
            galleryURi1 = uri;
        }else if (requestCode == FROM_CAMERA_2 ){
            galleryUri2 = uri;
        }


        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, requestCode);
    }

    private void pickImageFromGallery(int requestCode) {
        startActivityForResult(new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI),
                requestCode);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("requestCode:", requestCode + ",resultCode:" + resultCode);




        if(requestCode == FROM_CAMERA_1 && resultCode == RESULT_OK) {
            //mFeedbackBitmap = BitmapFactory.decodeFile(mUriImage.getPath());
            Bitmap mFeedbackBitmap = BitmapUtils.setImage(pickPic1, galleryURi1.getPath());
            if(mFeedbackBitmap == null) {
                // showToast("Image not found.");
                return;
            }
            Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScannerIntent.setData(galleryURi1);
            sendBroadcast(mediaScannerIntent);


        } else if(requestCode == FROM_GALLERY_1 && resultCode == FragmentActivity.RESULT_OK) {
            galleryURi1 = data.getData();


            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(galleryURi1, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            cursor.close();
            //mFeedbackBitmap = BitmapFactory.decodeFile(imagePath);
            Bitmap bitmap = BitmapUtils.setImage(pickPic1, imagePath);
            if(bitmap == null) {

                return;
            }

        }

        if(requestCode == FROM_CAMERA_2 && resultCode == RESULT_OK) {

            Bitmap mFeedbackBitmap = BitmapUtils.setImage(pickPic2, galleryUri2.getPath());
            if(mFeedbackBitmap == null) {

                return;
            }
            Intent mediaScannerIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScannerIntent.setData(galleryUri2);
            sendBroadcast(mediaScannerIntent);


        } else if(requestCode == FROM_GALLERY_2 && resultCode == FragmentActivity.RESULT_OK) {
            galleryUri2 = data.getData();


            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(galleryUri2, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            cursor.close();
            //mFeedbackBitmap = BitmapFactory.decodeFile(imagePath);
            Bitmap bitmap = BitmapUtils.setImage(pickPic2, imagePath);
            if(bitmap == null) {
                //showToast("Image not found.");
                return;
            }
            //mImgFeedback.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        }

        else if(resultCode == FragmentActivity.RESULT_OK && requestCode == SCAN_COMPLETE_2 && data != null) {
            if(data.getStringExtra("SCAN_RESULT") != null) {
                Log.d("SCAN", data.getStringExtra("SCAN_RESULT").toString());
                // mImgBarcodeCaptured.setVisibility(View.VISIBLE);
                // mBtnBarcode.setTag(data.getStringExtra("SCAN_RESULT").toString());
            }
        }
        else if(resultCode == FragmentActivity.RESULT_OK && requestCode == SCAN_COMPLETE_1 && data != null) {
            if(data.getStringExtra("SCAN_RESULT") != null) {
                Log.d("SCAN", data.getStringExtra("SCAN_RESULT").toString());
                // mImgBarcodeCaptured.setVisibility(View.VISIBLE);
                // mBtnBarcode.setTag(data.getStringExtra("SCAN_RESULT").toString());
            }
        }
    }

    private void showImagePicDialog(final  int requestCodeForGallery, final int requestCodeForCamera) {
        final Dialog dialog = new Dialog(this, R.style.DialogSlideAnim);
        dialog.setContentView(R.layout.dialog_image_picker);
        dialog.setCanceledOnTouchOutside(false);

        final TextView showGallery, showImage;
        final ImageView cancel;

        showGallery = (TextView)dialog.findViewById(R.id.txtOpenGallery);
        showImage = (TextView)dialog.findViewById(R.id.txtTakePhoto);

        cancel = (ImageView)dialog.findViewById(R.id.imgCancel);

        showGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGallery(requestCodeForGallery);
                dialog.dismiss();
            }
        });

        showImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                captureImage(requestCodeForCamera);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    EventPojo[] eventPojos1;




    private class GetEvent extends AsyncTask<Void,Void,Void>{



        @Override
        protected Void doInBackground(Void... params) {

            String stringURL = "http://neo.showcase-url.com/index.php/sync/get_events";

            int statusCode;
            try {
                URL url = new URL(stringURL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

                httpURLConnection.setRequestMethod("GET");


                statusCode = httpURLConnection.getResponseCode();

                if (statusCode == 200) {

                    InputStream inputStream = httpURLConnection.getInputStream();
                    putIntoDatabase(inputStream);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            init();
        }

        void putIntoDatabase(InputStream inputStream){
            eventPojos1 = new GsonBuilder().create().fromJson(new InputStreamReader(inputStream), EventPojo[].class);

            feedIntoDatabase(eventPojos1);
        }

        void feedIntoDatabase(EventPojo[] eventPojos){

            String query = " insert or replace into " + DatabaseUtils.EVENTS_TABLE + " (  " + DatabaseUtils.EVENT_ID + ", " + DatabaseUtils.EVENT_NAME + " ) values (?,?)";

            DatabaseHandler databaseHandler = new DatabaseHandler(EventActivity.this);
            DatabaseManager.initializeInstance(databaseHandler);

            SQLiteDatabase sqLiteDatabase = DatabaseManager.getInstance().openDatabase();

            SQLiteStatement sqLiteStatement = sqLiteDatabase.compileStatement(query);

            sqLiteDatabase.beginTransaction();
            for (int i = 0 ; i < eventPojos.length ;i++){
                sqLiteStatement.clearBindings();

                sqLiteStatement.bindLong(1,eventPojos[i].getId());
                sqLiteStatement.bindString(2,eventPojos[i].getName());

                sqLiteStatement.executeInsert();
            }

            sqLiteDatabase.setTransactionSuccessful();
            sqLiteDatabase.endTransaction();

            DatabaseManager.getInstance().closeDatabase();


        }
    }

    String visitorName;
    int eventId;
    private void putEventIntoDatabase(){

        if (eventName.getText().toString().length()>0) {






       /* if (!(eventName.getText().toString() != null && eventName.getText().toString().length()>1)) {
            Toast.makeText(EventActivity.this, "Please enter Visitor's name ", Toast.LENGTH_SHORT).show();}

        else  if (!(commentText.getText().toString() != null && commentText.getText().toString().length() > 1)){
            Toast.makeText(EventActivity.this, "Please enter your comments ", Toast.LENGTH_SHORT).show();}*/

/*
            if (!(commentText.getText().toString() != null && commentText.getText().toString().length() > 1)) {
                Toast.makeText(EventActivity.this, "Please enter Visitor's name ", Toast.LENGTH_SHORT).show();

            } else
                Toast.makeText(EventActivity.this, "Please enter your comments ", Toast.LENGTH_SHORT).show();

                //           Toast.makeText(EventActivity.this, "Please enter Visitor's name ", Toast.LENGTH_SHORT).show();

        } else

                Toast.makeText(EventActivity.this, "Please enter Visitor's name ", Toast.LENGTH_SHORT).show();*/

                Log.e("event ", " save button called");

                new AsyncTask<Void, Void, Void>() {
                    String commentText1;
                    String eventNameTxt;
                    ProgressDialog progressDialog;
                    int ratingBarCounter;

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
               /* visitorName = eventPojos.get[eventSpinner.getSelectedItemPosition()].getName();
                eventId = eventPojos[eventSpinner.getSelectedItemPosition()].getId();

                Log.e("event name","event detail " + eventPojos[eventSpinner.getSelectedItemPosition()].getName() + "  " +eventPojos[eventSpinner.getSelectedItemPosition()].getId());*/

                        visitorName = eventPojos.get(eventSpinner.getSelectedItemPosition()).getName();
                        eventId = eventPojos.get(eventSpinner.getSelectedItemPosition()).getId();
                        Log.e("event name", "event detail " + visitorName + "  " + eventId);
                        commentText1 = commentText.getText().toString();

                        eventNameTxt = eventName.getText().toString();

                        ratingBarCounter = (int) eventRatingBar.getRating();

                        Log.e("rating bar", " rating baar " + ratingBarCounter);
                        progressDialog = new ProgressDialog(EventActivity.this);
                        progressDialog.setMessage("Saving your data");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                    }

                    @Override
                    protected Void doInBackground(Void... params) {

                        SharedPreferences sharedPreferences = getSharedPreferences(Constant.SHARED_PREFERENCE_CONST, Context.MODE_PRIVATE);

                        int userId = sharedPreferences.getInt(Constant.SHARED_USER_ID, 0);
                        String userName = sharedPreferences.getString("userEmail", "");

                        int entryId = sharedPreferences.getInt("EntryId", 0);

                        entryId++;

                        Long time = System.currentTimeMillis();

                        String query = " insert  into " +
                                DatabaseUtils.VISITORS_INFO_TABLE + "( " +
                                DatabaseUtils.VISITOR_USER_ID_FK + " , " +
                                DatabaseUtils.VISITOR_USER_EMAIL + " , " +
                                DatabaseUtils.VISITORS_NAME + " , " +
                                DatabaseUtils.VISITORS_EVENT_NAME + " , " +
                                DatabaseUtils.VISITORS_EVENT_ID + ", " +
                                DatabaseUtils.VISITORS_EVENT_IMAGE_URL1 + ", " +
                                DatabaseUtils.VISITORS_EVENT_IMAGE_URL2 + ", " +
                                DatabaseUtils.VISITORS_EVENT_DATE + " , " +
                                DatabaseUtils.VISITORS_COMENTS_TEXT + " , " +
                                DatabaseUtils.VISITORS_IS_SYNCED + " , " +
                                DatabaseUtils.VISITORS_EVENT_RATE + " , " +
                                DatabaseUtils.VISTORS_ENTRY_ID + " ) values ( ?,?,?," +
                                "?,?,?," +
                                "?,?,?," +
                                "?,?,? )";

                        DatabaseHandler databaseHandler = new DatabaseHandler(EventActivity.this);
                        DatabaseManager.initializeInstance(databaseHandler);

                        SQLiteDatabase sqLiteDatabase = DatabaseManager.getInstance().openDatabase();
                        SQLiteStatement sqLiteStatement = sqLiteDatabase.compileStatement(query);

                        sqLiteStatement.clearBindings();

                        sqLiteStatement.bindLong(1, userId);
                        sqLiteStatement.bindString(2, userName);
                        sqLiteStatement.bindString(4, visitorName);
                        sqLiteStatement.bindString(3, eventNameTxt + "");
                        sqLiteStatement.bindLong(5, eventId);
                        if (galleryURi1 != null) {
                            sqLiteStatement.bindString(6, galleryURi1.toString());

                            Log.e(" sqlite gllery 1", " gallery 1 url : " + galleryURi1.toString());
                        } else
                            sqLiteStatement.bindString(6, "");
                        if (galleryUri2 != null) {
                            sqLiteStatement.bindString(7, galleryUri2.toString());

                            Log.e(" sqlite gllery 2", " gallery 2 url : " + galleryUri2.toString());
                        } else
                            sqLiteStatement.bindString(7, "");

                        sqLiteStatement.bindString(8, "" + time);
                        sqLiteStatement.bindString(9, "" + commentText1);
                        sqLiteStatement.bindLong(10, 0);
                        sqLiteStatement.bindLong(11, ratingBarCounter);
                        sqLiteStatement.bindLong(12, entryId);


                        sqLiteStatement.executeInsert();


                        SharedPreferences.Editor editor
                                = sharedPreferences.edit();

                        editor.putInt("EntryId", entryId);
                        editor.apply();
                        editor.commit();

                        DatabaseManager.getInstance().closeDatabase();

                        Log.e("background", " background closing ");

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        if (progressDialog.isShowing()) {
                            progressDialog.cancel();
                        }
                        Log.e("post", " post complete");
                        new AlertDialog.Builder(EventActivity.this)
                                .setTitle("Data Inserted")
                                .setMessage("You data is saved in database")
                                .setNeutralButton("Done", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        reset();
                                        dialog.dismiss();

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                }.execute();


        }
        else
            Toast.makeText(EventActivity.this, "Please enter Visitor's Name ", Toast.LENGTH_SHORT).show();


    }


    public void reset(){

        eventName.setText("");
        commentText.setText("");
        pickPic2.setImageResource(R.mipmap.placeholder);
        eventRatingBar.setRating(0);

        pickPic1.setImageResource(R.mipmap.placeholder);

    }
    @Override
    public void onClick(View v) {


        switch (v.getId()){

            case R.id.saveButton :

                Log.e("event ", " save button called");
                putEventIntoDatabase();
                break;

            case R.id.cancel :

                startActivity(new Intent(this,LoginScreen.class));
                finish();
                break;

            case R.id.findImageScreen1 :

                showImagePicDialog(FROM_GALLERY_1,FROM_CAMERA_1);

                break;

            case R.id.findImageScreen2 :
                showImagePicDialog(FROM_GALLERY_2, FROM_CAMERA_2);

                break;

            case R.id.list :

                startActivity(new Intent(this, EventListActivity.class));
                break;

            case R.id.sync :

                new startSync().execute();


        }
    }

    private class startSync extends AsyncTask<Void,Void,Void> {



        public startSync(){

        }
        ProgressDialog progressDialog;
        String message;
        int pos =0 ;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(EventActivity.this);

            progressDialog.setCancelable(false);
            progressDialog.setMessage(" Syncing data");
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            SQLiteDatabase sqLiteDatabase = new DatabaseHandler(EventActivity.this).getReadableDatabase();

            String query = " select * from " + DatabaseUtils.VISITORS_INFO_TABLE + " where "
                    +DatabaseUtils.VISITORS_IS_SYNCED + " = 0 ";

            final Cursor cursor = sqLiteDatabase.rawQuery(query,null);

            if (cursor != null && cursor.moveToFirst()){

                for (; pos <cursor.getCount() ;pos++){



                    /*new Thread(new Runnable() {
                        @Override
                        public void run() {*/
                            VisitorData visitorData = new VisitorData();
                            // pos = i;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.setMessage(" Syncing data " +(pos+1)+ "/" +cursor.getCount() );
                                }
                            });/*
                            if (cursor.moveToPosition(pos)) {*/
                                cursor.moveToPosition(pos);
                                visitorData.setVisitorname(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_NAME)));
                                visitorData.setComentText(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_COMENTS_TEXT)));
                                visitorData.setImageUrl1(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_EVENT_IMAGE_URL1)));
                                visitorData.setImageUrl2(cursor.getString(cursor.getColumnIndex(DatabaseUtils.VISITORS_EVENT_IMAGE_URL2)));
                                visitorData.setEventId(cursor.getInt(cursor.getColumnIndex(DatabaseUtils.VISITORS_EVENT_ID)));
                                visitorData.setEntryId(cursor.getInt(cursor.getColumnIndex(DatabaseUtils.VISTORS_ENTRY_ID)));
                                visitorData.setVisitorRating(cursor.getInt(cursor.getColumnIndex(DatabaseUtils.VISITORS_EVENT_RATE)));


                                SyncData syncData = new SyncData(visitorData, EventActivity.this);

                                message = syncData.startSync();
                            }

                      /*  }
                    }).start();*/
               // }

            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (progressDialog.isShowing()){
                progressDialog.dismiss();
            }

            new AlertDialog.Builder(EventActivity.this).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which){
                    dialog.dismiss();
                }
            }).setMessage(message).show();


        }
    }
}
