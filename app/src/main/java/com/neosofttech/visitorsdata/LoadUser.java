package com.neosofttech.visitorsdata;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.neosofttech.visitorsdata.databaseOp.DatabaseHandler;
import com.neosofttech.visitorsdata.databaseOp.DatabaseManager;
import com.neosofttech.visitorsdata.databaseOp.DatabaseUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by webwerks on 6/10/15.
 */
public class LoadUser  {

    public LoadUser(Context context){
        this.context = context;
        doInBackground();
    }
    ProgressDialog progressDialog;

    Context context ;
    int statusCode;


    protected Void doInBackground() {

        String stringURL = "http://neo.showcase-url.com/sync/get_users";

        try {
            URL url = new URL(stringURL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            httpURLConnection.setRequestMethod("GET");


            statusCode = httpURLConnection.getResponseCode();

            if (statusCode == 200) {

                InputStream inputStream = httpURLConnection.getInputStream();
                putIntoDatabase(inputStream);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    private void putIntoDatabase(InputStream inputStream){

        UserPojo[] userPojos = new GsonBuilder().create().fromJson(new InputStreamReader(inputStream), UserPojo[].class);

        feedIntoDatabase(userPojos);

    }

    private void feedIntoDatabase(UserPojo[ ] userPojos) {

        DatabaseHandler databaseHandler = new DatabaseHandler(context);
        DatabaseManager.initializeInstance(databaseHandler);


        SQLiteDatabase sqLiteDatabase = DatabaseManager.getInstance().openDatabase();
        if (userPojos.length>1) {

            String query = " delete from " + DatabaseUtils.USERS_TABLE;
            sqLiteDatabase.execSQL(query);
        }

        String query = " insert or replace into " + DatabaseUtils.USERS_TABLE + " ( " +
                DatabaseUtils.USERS_ID + " , "
                + DatabaseUtils.USER_EMAIL_ID + " , "
                +DatabaseUtils.USER_PASSWORD+ " ) values ( ?,?,?)";

        SQLiteStatement sqLiteStatement = sqLiteDatabase.compileStatement(query);

        sqLiteDatabase.beginTransaction();

        for (int i = 0 ; i < userPojos.length ; i++){

            sqLiteStatement.clearBindings();

            sqLiteStatement.bindLong(1, Long.parseLong(userPojos[i].getId().trim()));
            sqLiteStatement.bindString(2, userPojos[i].getEmail());

            sqLiteStatement.bindString(3, userPojos[i].getPassword());

            Log.e(" user ", Long.parseLong(userPojos[i].getId().trim()) + "   " + userPojos[i].getEmail() + "   " + userPojos[i].getPassword());

            sqLiteStatement.executeInsert();


        }

        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();


        DatabaseManager.getInstance().closeDatabase();


    }



}