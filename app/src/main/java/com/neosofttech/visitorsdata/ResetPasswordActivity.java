package com.neosofttech.visitorsdata;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.neosofttech.visitorsdata.databaseOp.DatabaseHandler;
import com.neosofttech.visitorsdata.databaseOp.DatabaseManager;
import com.neosofttech.visitorsdata.databaseOp.DatabaseUtils;

/**
 * Created by webwerks on 6/10/15.
 */
public class ResetPasswordActivity extends Activity implements View.OnClickListener {


    EditText userName,old_password,reEnterpassword,new_password;
    Button submitButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.reset_password_layout);

        userName = (EditText)findViewById(R.id.emailId);
        old_password = (EditText)findViewById(R.id.oldPassword);
        new_password = (EditText)findViewById(R.id.resetPassword1);
        reEnterpassword = (EditText)findViewById(R.id.resetPassword2);


        submitButton = (Button)findViewById(R.id.button);

        submitButton.setOnClickListener(this);

    }


    public void callBackPressed(){
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        validateBoxesvalue();
    }

    int userId = 0;

    private void showError(String query){

        new AlertDialog.Builder(this).setMessage(query).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    String oldPassword,newPassword;
    private void validateBoxesvalue() {

        if (!userName.getText().toString().isEmpty()){

            String query = "select * from "+ DatabaseUtils.VISITORS_INFO_TABLE + " where "+DatabaseUtils.VISITOR_USER_EMAIL + " = '" + userName.getText().toString() + "' ";

            SQLiteDatabase sqLiteDatabase = new DatabaseHandler(this).getReadableDatabase();

            Cursor cursor
                    = sqLiteDatabase.rawQuery(query,null);

            if (cursor.moveToFirst()){

                userId = cursor.getInt(cursor.getColumnIndex(DatabaseUtils.VISITOR_USER_ID_FK));

            }
        }else{
            showError(" wrong user name please change");
            return;
        }


        if (!new_password.getText().toString().equalsIgnoreCase(reEnterpassword.getText().toString())){
            showError("new Password and reEnter password does not work");
            return;
        }

        newPassword = new_password.getText().toString();

        oldPassword = old_password.getText().toString();

        new CallResetApi().execute();


    }

    private class CallResetApi extends AsyncTask<Void,Void,Void>{

        ProgressDialog  progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(ResetPasswordActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (progressDialog.isShowing()){
                progressDialog.dismiss();
            }

            Toast.makeText(ResetPasswordActivity.this," user : " +text, Toast.LENGTH_SHORT).show();
            callBackPressed();
        }
        String text ;
        @Override
        protected Void doInBackground(Void... params) {

            ResetPasswordApi resetPasswordApi = new ResetPasswordApi();
             text= resetPasswordApi.resetPassword(userId,oldPassword,newPassword);
            LoadUser loadUser =new LoadUser(ResetPasswordActivity.this);


            return null;
        }
    }
}
