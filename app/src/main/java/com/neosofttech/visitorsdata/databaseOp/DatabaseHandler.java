package com.neosofttech.visitorsdata.databaseOp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

    /*
        * const char *sql_student = "CREATE TABLE IF NOT EXISTS Users (userId integer PRIMARY KEY,email varchar(120),password varchar(120))";

                const char *sql_student1 = "CREATE TABLE IF NOT EXISTS VisitorsInfo (userId integer ,
                Useremail varchar(255),Vname varchar(255),Eventname varchar(255),
                eventId varchar(10),IMAGE1 BLOB,IMAGE2 BLOB,
                Eventdate  varchar(255), comments text, isSynced varchar(5), entryId integer)";

                const char *sql_event= "CREATE TABLE IF NOT EXISTS Events (eventId integer ,EventName varchar(255))";
        * */

    public DatabaseHandler(Context context) {
        super(context, DatabaseUtils.DATABASE_NAME, null, DatabaseUtils.DATABASE_VERSION);
    }

    public static final String USER_TABLE = "CREATE TABLE if not exists  " +DatabaseUtils.USERS_TABLE + " ( " +
            DatabaseUtils.USERS_ID + " integer primary key , " +
            DatabaseUtils.USER_EMAIL_ID + " TEXT , " +
            DatabaseUtils.USER_PASSWORD + " text )";

    public static final String VISITOR_INFO_TABLE = " CREATE TABLE IF NOT EXISTS " + DatabaseUtils.VISITORS_INFO_TABLE + " ( "+
            DatabaseUtils.VISITOR_USER_ID_FK + " integer , " +
            DatabaseUtils.VISITOR_USER_EMAIL + " text , " +
            DatabaseUtils.VISITORS_NAME + " text , " +
            DatabaseUtils.VISITORS_EVENT_NAME + " text , " +
            DatabaseUtils.VISITORS_EVENT_ID + " text, "+
            DatabaseUtils.VISITORS_EVENT_IMAGE_URL1 + " text, "+
            DatabaseUtils.VISITORS_EVENT_IMAGE_URL2 + " text, "+
            DatabaseUtils.VISITORS_EVENT_DATE+ " text, " +
            DatabaseUtils.VISITORS_COMENTS_TEXT + " text, "+
            DatabaseUtils.VISITORS_IS_SYNCED + " integer , "+
            DatabaseUtils.VISITORS_EVENT_RATE+ " integer , "+
            DatabaseUtils.VISTORS_ENTRY_ID + " integer )";

    public static final String EVENT_TABLE = " CREATE TABLE IF NOT EXISTS " + DatabaseUtils.EVENTS_TABLE + " ( "+
            DatabaseUtils.EVENT_ID + " integer primary key , " +
            DatabaseUtils.EVENT_NAME + " text ) ";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(USER_TABLE);
        db.execSQL(VISITOR_INFO_TABLE);
        db.execSQL(EVENT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        try {
            if ((newVersion == 2) && (newVersion > oldVersion)) {
                db.execSQL("Alter table " + DatabaseUtils.VISITORS_INFO_TABLE + " Add  " + DatabaseUtils.VISITORS_EVENT_RATE + " integer ");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}