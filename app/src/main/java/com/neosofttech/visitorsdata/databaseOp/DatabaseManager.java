package com.neosofttech.visitorsdata.databaseOp;

import android.database.sqlite.SQLiteDatabase;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by webwerks on 16/9/15.
 */
public class DatabaseManager {

    private static DatabaseHandler mDatabaseHandler;
    private static DatabaseManager instance;
    private AtomicInteger mOpenCounter = new AtomicInteger(0);
    private SQLiteDatabase mSqLiteDatabase;

    public static synchronized void initializeInstance(DatabaseHandler databaseHandler) {

        if (instance == null) {
            instance = new DatabaseManager();
            mDatabaseHandler = databaseHandler;
        }
    }

    public static synchronized DatabaseManager getInstance() {

        if (instance == null) {
            throw new IllegalStateException(DatabaseManager.class.getSimpleName() + " is not initialize please call initializeInstance(...) method fist.");

        }
        return instance;
    }

    public synchronized SQLiteDatabase openDatabase() {

        if (mOpenCounter.incrementAndGet() == 1) {

            // opening database for inserting or updating.
            mSqLiteDatabase = mDatabaseHandler.getWritableDatabase();
        }

        return mSqLiteDatabase;
    }


    public synchronized void closeDatabase() {

        if (mOpenCounter.decrementAndGet() == 0) {
            mSqLiteDatabase.close();
        }

    }
}