package com.neosofttech.visitorsdata.databaseOp;

/**
 * Created by webwerks on 16/9/15.
 */
public class DatabaseUtils {

    public static int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "neofsofttech.db";


    /*
    * const char *sql_student = "CREATE TABLE IF NOT EXISTS Users (userId integer PRIMARY KEY,email varchar(120),password varchar(120))";

            const char *sql_student1 = "CREATE TABLE IF NOT EXISTS VisitorsInfo (userId integer ,Useremail varchar(255),Vname varchar(255),Eventname varchar(255),
            eventId varchar(10),IMAGE1 BLOB,IMAGE2 BLOB,
            Eventdate  varchar(255), comments text, isSynced varchar(5), entryId integer)";

            const char *sql_event= "CREATE TABLE IF NOT EXISTS Events (eventId integer ,EventName varchar(255))";



    * */


    public static final String USERS_TABLE = "userTable",
                               USERS_ID = "userId",
                               USER_EMAIL_ID = "userEmailId",
                               USER_PASSWORD = "userPassword",

                               VISITORS_INFO_TABLE = "visitorInfo",
                               VISITOR_USER_ID_FK = "userdIdFk",
                               VISITOR_USER_EMAIL = "userEmail",
                               VISITORS_NAME = "visitorsName",
                               VISITORS_EVENT_NAME = "visitorsEventName",
                               VISITORS_EVENT_ID = "visitorsEventID",
                               VISITORS_EVENT_IMAGE_URL1 = "visitorsEventUrl1",
                               VISITORS_EVENT_IMAGE_URL2 = "visitorsEventUrl2",
                               VISITORS_EVENT_DATE = "visitorsEventDate",
                               VISITORS_COMENTS_TEXT = "visitorsComent",
                               VISITORS_IS_SYNCED = "isSynced",
                               VISTORS_ENTRY_ID = "entryId",
                               VISITORS_EVENT_RATE = "eventRating",

                               EVENTS_TABLE = "eventTable",
                               EVENT_ID = "eventId",
                               EVENT_NAME = "eventName";






}
