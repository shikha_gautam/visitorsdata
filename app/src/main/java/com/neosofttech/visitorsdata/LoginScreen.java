package com.neosofttech.visitorsdata;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.neosofttech.visitorsdata.databaseOp.DatabaseHandler;
import com.neosofttech.visitorsdata.databaseOp.DatabaseManager;
import com.neosofttech.visitorsdata.databaseOp.DatabaseUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Target;
import java.net.MalformedURLException;

public class LoginScreen extends AppCompatActivity implements View.OnClickListener {


    SharedPreferences sharedPreferences ;
    Button button;
    EditText userEditText, passEditText;
    CheckBox isUserIdChecked;
    View parentView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_login);

        sharedPreferences = getSharedPreferences("VisitorData", Context.MODE_PRIVATE);

        int isDataStored = sharedPreferences.getInt("IS_DATA_STORED", 0 );

        if (isDataStored == 0){

            new PutIntoDatabase().execute();
        }

        parentView = findViewById(R.id.framelayout);
        userEditText = (EditText)findViewById(R.id.emailId);
        passEditText = (EditText)findViewById(R.id.password);

        button = (Button)findViewById(R.id.button);

        isUserIdChecked = (CheckBox)findViewById(R.id.userSaved);


        boolean isChecked = sharedPreferences.getBoolean("isSaved", false);


        if (isChecked){
            String userName = sharedPreferences.getString(Constant.USER_EMAIL_ID,"");
            String password = sharedPreferences.getString(Constant.USER_PASSWORD, "");

            userEditText.setText(userName);
            passEditText.setText(password);
            isUserIdChecked.setChecked(isChecked);

        }

        button.setOnClickListener(this);


        parentView.post(new Runnable() {
            @Override
            public void run() {
              //  convertTopdf();
            }
        });
    }


    public void forgotPassword(View v){
        startActivity(new Intent(this,ForgotPasswordActivity.class));
    }

    public  void resetPassword(View v){
        startActivity(new Intent(this,ResetPasswordActivity.class));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login_screen, menu);
        return true;
    }

    public static int UserId;
    @Override
    public void onClick(View v) {

        String userName = userEditText.getText().toString();
        String pass = passEditText.getText().toString();

        String newpass =  NetworkUtil.md5(pass);


        SQLiteDatabase sqLiteDatabase = new DatabaseHandler(this).getReadableDatabase();

        String query = " select * from "+ DatabaseUtils.USERS_TABLE + " where "  + DatabaseUtils.USER_EMAIL_ID + " = '" +
                                    userName + "' and " + DatabaseUtils.USER_PASSWORD + " = '" + newpass +"' ";


        Log.e(" query" ,query );
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);

        if (cursor!= null && (cursor.getCount()==1)){

            SharedPreferences.Editor editor
                     =sharedPreferences.edit();
            cursor.moveToFirst();

            editor.putInt("userId",UserId= cursor.getInt(cursor.getColumnIndex(DatabaseUtils.USERS_ID)));
            editor.putString("userEmail", cursor.getString(cursor.getColumnIndex(DatabaseUtils.USER_EMAIL_ID)));
            editor.putString(Constant.USER_PASSWORD,pass);

            editor.putBoolean("isSaved", isUserIdChecked.isChecked());

            editor.apply();
            editor.commit();


            startActivity(new Intent(this, EventActivity.class));
            finish();

        }else{
            Toast.makeText(this," logged in fail ", Toast.LENGTH_LONG).show();
        }

    }


   /* private void convertTopdf(){
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {

        }

//Create a directory for your PDF
        File pdfDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS), "MyApp");
        if (!pdfDir.exists()){
            pdfDir.mkdir();
        }*/

//Then take the screen shot
      /*  Bitmap screen; View v1 = parentView.getRootView();
        v1.setDrawingCacheEnabled(true);
        screen = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);

//Now create the name of your PDF file that you will generate
        File pdfFile = new File(pdfDir, "myPdfFile.pdf");

        Log.e("absolute ", " get absolute path " + pdfFile.getAbsolutePath());*/


        /*LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        LinearLayout root = (LinearLayout) inflater.inflate
                (R.layout.activity_login, null);*//* //RelativeLayout is root view of my UI(xml) file.*//*
        root.setDrawingCacheEnabled(true);
        screen= getBitmapFromView(this.getWindow().findViewById
                (R.id.fullScreen));*/


        /*try {
            Document document = new Document();

            PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
            document.open();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            screen.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            addImage(document,byteArray);
            document.close();

            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(new File(pdfDir, "myPdfFile.pdf"));
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }*/


    /*private static void addImage(Document document,byte[] byteArray)
    {
        Image image = null;
        try
        {
            image = Image.getInstance(byteArray);
        }
        catch (BadElementException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (MalformedURLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // image.scaleAbsolute(150f, 150f);
        try
        {
            document.add(image);
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public  Bitmap getBitmapFromView(View v) {
        Bitmap b = Bitmap.createBitmap( v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
        v.draw(c);
        return b;
    }*/

    private class PutIntoDatabase extends AsyncTask<Void,Void,Void>{

        Context context = LoginScreen.this;
        @Override
        protected Void doInBackground(Void... params) {

            DatabaseHandler databaseHandler = new DatabaseHandler(context);
            DatabaseManager.initializeInstance(databaseHandler);

           /* SQLiteDatabase sqLiteDatabase = DatabaseManager.getInstance().openDatabase();
            *//*
            "INSERT INTO Users (userId, email,password) VALUES (1, 'poonam@neosofttech.com', 'NeoSOFT123!@#')
            ,(2, 'nikhil@neosofttech.com', 'neoWerks'),
            (3, 'nishant@neosofttech.com', 'webSoft'),
            (4, 'sameer@neosofttech.com', 'terraneowerks')";
             *//*
            String insertquery = " insert or replace into " + DatabaseUtils.USERS_TABLE + " ( " +
                                   DatabaseUtils.USERS_ID + " , " + DatabaseUtils.USER_EMAIL_ID + " , " +DatabaseUtils.USER_PASSWORD+ " ) values (1, 'poonam@neosofttech.com', 'NeoSOFT123!@#')" +
                    ",(2, 'nikhil@neosofttech.com', 'neoWerks')," +
                    "(3, 'nishant@neosofttech.com', 'webSoft')," +
                    "(4, 'sameer@neosofttech.com', 'terraneowerks')," +
                    "(5,'ashfaque.sayed@wwindia.com','123456')," +
                    "(6,'rajesh.singh@wwindia.com','123456')," +
                    "(7,'kaushal.vedant@wwindia.com','kaushal_webwerksindia') ";

            sqLiteDatabase.execSQL(insertquery);

            DatabaseManager.getInstance().closeDatabase();*/

            SharedPreferences.Editor editor
                     = sharedPreferences.edit();
            editor.putInt("IS_DATA_STORED", 1);
            editor.apply();

            editor.commit();


            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
