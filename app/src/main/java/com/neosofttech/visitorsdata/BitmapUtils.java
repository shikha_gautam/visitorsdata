package com.neosofttech.visitorsdata;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by webwerks on 18/9/15.
 */
public class BitmapUtils {

    private static Bitmap bitmapStaticMap = null;

    public static Bitmap setImage(ImageView imageView, String currentPhotoPath) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        if (targetH == 0) {
            targetH = 300;
        }
        if (targetW == 0) {
            targetW = 300;
        }
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions);

        bitmap = checkImageOrientation(bitmap, currentPhotoPath);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
        return checkImageOrientation(bitmap, currentPhotoPath);
    }



    /*public static Bitmap getFromFile(String path,int size) {
        BitmapFactory.Options opt = new Options();

        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path,opt);

		*//*int sampleSize = 0;
        if (opt.outHeight > 400 || opt.outWidth > 400) {
			if (opt.outHeight > opt.outWidth) {
				sampleSize = opt.outHeight / 200;
			} else {
				sampleSize = opt.outWidth / 200;

			}
		}*//*
        // bmp.recycle();
        opt.inSampleSize = BitmapUtils.calculateInSampleSize(opt,size, size);
        opt.inJustDecodeBounds = false;
        opt.inPreferredConfig = Config.RGB_565;
        opt.inDither = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path,opt);

        return checkImageOrientation(bitmap, path);
    }*/

    private static Bitmap checkImageOrientation(Bitmap bitmap, String path) {

        ExifInterface ei;
        try {
            ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);


                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    private static Bitmap rotateImage(Bitmap source, float angle) {

        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                    matrix, true);
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
        }
        if (bitmap == null) {
            return source;
        } else {
            // source.recycle();
            source = null;
            return bitmap;
        }
    }

    public static Bitmap getBitmapImage(String currentPhotoPath) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions);


        // Determine how much to scale down the image


        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;

        bmOptions.inPurgeable = true;
        final int REQUIRED_WIDTH = 300;
        final int REQUIRED_HIGHT = 300;
        //Find the correct scale value. It should be the power of 2.
        int scale = 1;
        while (bmOptions.outWidth / scale / 2 >= REQUIRED_WIDTH && bmOptions.outHeight / scale / 2 >= REQUIRED_HIGHT)
            scale *= 2;

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        /*try {*/
            return BitmapFactory.decodeFile(currentPhotoPath, bmOptions);

        /*} catch (OutOfMemoryError e) {

        }*/


    }

    public static Bitmap decodeFile(File f, int WIDTH, int HIGHT) {
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    public static void setImageFromUrl(final ImageView imageView, final String strUrl) {
        // Get the dimensions of the View
        int targetW = 50;
        int targetH = 50;

        // Get the dimensions of the bitmap
        final BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        //Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        final Handler handler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                imageView.setImageBitmap(bitmapStaticMap);
            }

            ;
        };
        new Thread() {
            public void run() {
                getImage(strUrl, bmOptions);
                handler.sendEmptyMessage(0);
            }

            ;
        }.start();
    }

    private static void getImage(String strUrl, BitmapFactory.Options bmOptions) {
        try {

            if (strUrl != null && !strUrl.equals("")) {
                URL url = null;
                URLConnection urlCon = null;
                InputStream inStream = null;
                Log.e("StaticMap", strUrl);

                try {
                    url = new URL(strUrl);
                    urlCon = url.openConnection();
                    inStream = urlCon.getInputStream();
                    bitmapStaticMap = BitmapFactory.decodeStream(inStream, null, bmOptions);
                    inStream.close();
                } catch (MalformedURLException e2) {
                    Log.e("PlaceLocatorWebService", "" + e2);
                } catch (IOException e) {
                    Log.e("PlaceLocatorWebService", "" + e);
                }
            } else {
                Log.e("TEST", "map null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}