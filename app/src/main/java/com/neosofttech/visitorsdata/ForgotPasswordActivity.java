package com.neosofttech.visitorsdata;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;



/**
 * Created by webwerks on 6/10/15.
 */
public class ForgotPasswordActivity extends Activity {

    Button submitButton;
    EditText emailId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        emailId = (EditText)findViewById(R.id.emailId);

        submitButton = (Button)findViewById(R.id.button);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!emailId.getText().toString().isEmpty()){
                    new ForgotPasswordApi1().execute();
                }
            }
        });
    }

    public void callBackPressed(){
        super.onBackPressed();
    }
    class ForgotPasswordApi1 extends AsyncTask<Void,Void,Void>{


        ProgressDialog progressDialog;
        ForgotPasswordApi forgotPasswordApi;
        String text ;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(ForgotPasswordActivity.this);
            progressDialog.setMessage(" Mailing your password to your email id");
            progressDialog.show();
            text = emailId.getText().toString();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()){
                progressDialog.dismiss();

            }
            callBackPressed();
        }

        @Override
        protected Void doInBackground(Void... params) {

            forgotPasswordApi = new ForgotPasswordApi();
            forgotPasswordApi.callUrl(text);

            return null;
        }
    }
}
