package com.neosofttech.visitorsdata;

/**
 * Created by webwerks on 18/9/15.
 */
public class Constant {

    public static final String SHARED_PREFERENCE_CONST = "VisitorData",
                               SHARED_USER_ID = "userId",
                               USER_EMAIL_ID = "userEmail",
                               USER_PASSWORD = "PASSWORD" ;
}
