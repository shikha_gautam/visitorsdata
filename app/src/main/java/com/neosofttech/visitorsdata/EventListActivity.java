package com.neosofttech.visitorsdata;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.neosofttech.visitorsdata.databaseOp.DatabaseHandler;
import com.neosofttech.visitorsdata.databaseOp.DatabaseUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by webwerks on 18/9/15.
 */
public class EventListActivity extends Activity {


    ListView listView;
    List<String> eventNames;
    HashMap<String, String> syncValue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_activity_layout);



        findViewById(R.id.imgBack).setOnClickListener(new
                                                               View.OnClickListener() {
                                                                   @Override
                                                                   public void onClick(View v) {
                                                                       onBackPressed();
                                                                   }
                                                               });
        listView = (ListView)findViewById(R.id.list);

        SharedPreferences sharedPreferences = getSharedPreferences(Constant.SHARED_PREFERENCE_CONST,Context.MODE_PRIVATE);

        String emailId = sharedPreferences.getString("userEmail", "");

        String text = emailId.split("@")[0];


        if (text.length()>8){

            ((TextView) findViewById(R.id.txtUserName)).setText(text.substring(0,8)+"...");
        }else{
            ((TextView) findViewById(R.id.txtUserName)).setText(text);
        }

        findViewById(R.id.txtUserName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        SQLiteDatabase sqLiteDatabase = new DatabaseHandler(this).getReadableDatabase();

        eventNames = new ArrayList<>();

        syncValue = new HashMap<>();


        String query = " select * from " + DatabaseUtils.EVENTS_TABLE + " group by " + DatabaseUtils.EVENT_NAME;

        String syncQuery = " select * from  " + DatabaseUtils.VISITORS_INFO_TABLE + " where " + DatabaseUtils.VISITORS_EVENT_ID + " = ?  and " + DatabaseUtils.VISITOR_USER_ID_FK + " = " + LoginScreen.UserId ;

        String isSyncQuery = " select * from " + DatabaseUtils.VISITORS_INFO_TABLE + " where " + DatabaseUtils.VISITORS_EVENT_ID+ " = ?  and " + DatabaseUtils.VISITORS_IS_SYNCED + " = 1 and " + DatabaseUtils.VISITOR_USER_ID_FK + " = " + LoginScreen.UserId ;
        Cursor cursor1,cursor2;
        Cursor cursor
                 = sqLiteDatabase.rawQuery(query,null);

        if (cursor!=null && cursor.moveToFirst()){

            for (int i = 0 ; i < cursor.getCount() ; i++){

                cursor.moveToPosition(i);
                eventNames.add(cursor.getString(cursor.getColumnIndex(DatabaseUtils.EVENT_NAME)));
                cursor1 = sqLiteDatabase.rawQuery(syncQuery ,new String[]{""+cursor.getInt(cursor.getColumnIndex(DatabaseUtils.EVENT_ID))});
                cursor2 = sqLiteDatabase.rawQuery(isSyncQuery, new String []{""+cursor.getInt(cursor.getColumnIndex(DatabaseUtils.EVENT_ID))});

                syncValue.put(eventNames.get(i),cursor1.getCount() + " &&&& " +cursor2.getCount() );



            }
        }


        ListItemAdapter listItemAdapter = new ListItemAdapter(EventListActivity.this,eventNames);

        listView.setAdapter(listItemAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent
                         = new Intent(EventListActivity.this,EventDetailActivity.class);

                intent.putExtra("EventName", eventNames.get(position));
                startActivity(intent);
            }
        });
    }



    private class ListItemAdapter extends BaseAdapter {

        private Context mContext;
        private List<String> list;
        private LayoutInflater mInflater;

        public ListItemAdapter(Context mContext, List<String> dataList) {
            this.mContext=mContext;
            this.list=dataList;
            mInflater= (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView==null){
                convertView=mInflater.inflate(R.layout.layout_list_item,null);
            }
            final TextView txtInfo= (TextView) convertView.findViewById(R.id.txtInfo);
            ImageView imgInfo= (ImageView) convertView.findViewById(R.id.imgInfo);

            txtInfo.setText(list.get(position));
            imgInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("TAG", "img clicked");
                    final Dialog dialogInfo=new Dialog(mContext);
                    dialogInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogInfo.setContentView(R.layout.layout_dialog);
                    dialogInfo.show();

                    TextView txtEventPlace= (TextView) dialogInfo.findViewById(R.id.txtEventPlace);
                    TextView txtSyncInfo= (TextView) dialogInfo.findViewById(R.id.txtSyncInfo);
                    TextView txtTotalInfo= (TextView) dialogInfo.findViewById(R.id.txtTotalInfo);
                    txtSyncInfo.setText(syncValue.get(list.get(position)).split(" &&&&")[1]);
                    txtTotalInfo.setText(syncValue.get(list.get(position)).split(" &&&&")[0]);
                    txtEventPlace.setText(txtInfo.getText().toString());
                    dialogInfo.findViewById(R.id.txtOK).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogInfo.dismiss();
                        }
                    });
                }
            });


            return convertView;
        }


    }

}

