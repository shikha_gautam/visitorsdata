package com.neosofttech.visitorsdata;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.neosofttech.visitorsdata.databaseOp.DatabaseHandler;
import com.neosofttech.visitorsdata.databaseOp.DatabaseManager;
import com.neosofttech.visitorsdata.databaseOp.DatabaseUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class SyncData {

    VisitorData visitorData;
    Context context;

    public SyncData(VisitorData visitorData, Context context) {

        this.visitorData = visitorData;
        this.context = context;
        //startSync();

    }


    public static enum ContentType {

        JSON("application/json"), MULTIPART_FORM("multipart/form-data"), FORM_ENCODED("application/x-www-form-urlencoded"), TEXT_XML("text/xml");

        private final String type;

        private ContentType(String type) {
            this.type = type;
        }


        @Override
        public String toString() {
            return type;
        }
    }

    public String startSync() {

        String stringUrl = "http://neo.showcase-url.com/sync";


        try {


            SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.SHARED_PREFERENCE_CONST, Context.MODE_PRIVATE);
            int userId = sharedPreferences.getInt(Constant.SHARED_USER_ID, 0);
            URL url = new URL(stringUrl);

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            httpURLConnection.setConnectTimeout(2000);

            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.addRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            httpURLConnection.setRequestProperty("charset", "utf-8");
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("name", visitorData.getVisitorname()));

            params.add(new BasicNameValuePair("event_id", visitorData.getEventId() + ""));

            params.add(new BasicNameValuePair("user_id", "" + userId));
            params.add(new BasicNameValuePair("comments", visitorData.getComentText() + ""));
            params.add(new BasicNameValuePair("photo1", getBase64From(visitorData.getImageUrl1()) + ""));
            params.add(new BasicNameValuePair("photo2", getBase64From(visitorData.getImageUrl2()) + ""));

            Log.e("event id ", " event id " + visitorData.getEventId());
            Log.e("name", visitorData.getVisitorname() + "");
            Log.e("user_id", "" + userId);


            OutputStream os = httpURLConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(params));

            writer.flush();
            writer.close();
            os.close();

            httpURLConnection.connect();

            int statusCode = httpURLConnection.getResponseCode();

            if (statusCode == 200) {
                InputStream inputStream = httpURLConnection.getInputStream();

                return feedIntoDatabase(inputStream);
            } else {
                Log.e("status code ", " status code " + statusCode);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Oops something went wrong.... please try after some time";
    }

    public String getData(InputStream inputStream) throws IOException {


        String readed = null;

        //  GZIPInputStream gzis = new GZIPInputStream(stream);


        InputStreamReader reader = new InputStreamReader(inputStream);
        BufferedReader in = new BufferedReader(reader);
        StringBuffer respBuffer = new StringBuffer();
        while ((readed = in.readLine()) != null) {
            respBuffer.append(readed);
            //Log.e("READ STRING",readed);
        }

        Log.e("Input stream data", respBuffer.toString());


        return respBuffer.toString();
    }

    private String feedIntoDatabase(InputStream inputStream) throws IOException {

        // Log.e(" input stream ", " inputStream " + getData(inputStream));

        Example example
                = new GsonBuilder().create().fromJson(new InputStreamReader(inputStream), Example.class);

        //Log.e("Example data ", example.getData())
        Log.e("example data ", example.getData().getCardId() + "   : " + example.getData().getCardName() + "  " + example.getStatus() + "  " + example.getMessage());

        if (example.getStatus()) {
            String qury = " update " + DatabaseUtils.VISITORS_INFO_TABLE + " set " +

                    DatabaseUtils.VISTORS_ENTRY_ID + " = " + example.getData().getCardId() + " , " +

                    DatabaseUtils.VISITORS_IS_SYNCED + " = 1  " +

                    " where " + DatabaseUtils.VISITORS_EVENT_ID + " = " + visitorData.getEventId() + " and " +
                    DatabaseUtils.VISTORS_ENTRY_ID + " = " + visitorData.getEntryId();

            DatabaseHandler databaseHandler = new DatabaseHandler(context);
            DatabaseManager.initializeInstance(databaseHandler);
            SQLiteDatabase sqLiteDatabase = DatabaseManager.getInstance().openDatabase();

            sqLiteDatabase.execSQL(qury);

            DatabaseManager.getInstance().closeDatabase();
        }

        return example.getMessage();

    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {

        StringBuilder result = new StringBuilder();

        boolean first = true;

        for (NameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");

            result.append(URLEncoder.encode(pair.getValue() == null ? "" : pair.getValue(), "UTF-8"));
        }

        Log.e(" param value ", result.toString());
        return result.toString();
    }


    private String getBase64From(String imagePath) {
        try {
            if (imagePath == null || imagePath.length() < 5) {
                return "";
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            getImageUrl(imagePath).compress(Bitmap.CompressFormat.PNG, 60, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();

            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

            Log.e("encode base64", "base 64 image : " + encoded);

            return encoded;
        } catch (Exception e) {

        }
        return "";
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        try {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;
            // CREATE A MATRIX FOR THE MANIPULATION
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight);

            // "RECREATE" THE NEW BITMAP
            Bitmap resizedBitmap = Bitmap.createBitmap(
                    bm, 0, 0, width, height, matrix, false);
            bm.recycle();
            return resizedBitmap;
        } catch (Exception e) {

        }
        return bm;
    }

    private Bitmap getImageUrl(String imageUrl) {
        if (imageUrl.contains("file://")) {
            imageUrl = imageUrl.replace("file://", "");
            return getResizedBitmap(BitmapUtils.getBitmapImage(imageUrl), 300, 300);

        } else if (imageUrl.contains("content:/")) {
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(Uri.parse(imageUrl), filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));
            cursor.close();
            //mFeedbackBitmap = BitmapFactory.decodeFile(imagePath);
            return getResizedBitmap(BitmapUtils.getBitmapImage(imagePath), 300, 300);
        }
        return null;
    }
}
