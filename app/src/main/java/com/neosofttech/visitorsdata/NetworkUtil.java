package com.neosofttech.visitorsdata;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.security.MessageDigest;

/**
 * Created by webwerks on 29/9/15.
 */
public class NetworkUtil {

    public static boolean isNetworkAvailable(Context context) {


        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String md5(String password){
       try {
           MessageDigest md = MessageDigest.getInstance("MD5");

           md.update(password.getBytes());

           byte byteData[] = md.digest();

           StringBuffer sb = new StringBuffer();
           for (int i = 0; i < byteData.length; i++)
               sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));


           return sb.toString();
       }catch (Exception e){
           e.printStackTrace();
       }

        return password;
    }
}
